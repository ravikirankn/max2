package services;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import pojos.Person;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class PersonServicesTest {

  PersonServices personServices = null;

  @Before
  public void setup() {
    personServices = new PersonServices();
  }

  Person person1 = new Person("Donald", "Duck", "", "99999-1234", "703 955 0373", "Golden");

  @Test
  public void testGetColorToPersonCountJSONForEmptyList() throws JsonProcessingException {
    List<Person> persons = new ArrayList<>();
    assertEquals(0, personServices.getColorToPersonCountJSON(persons).size());
  }

  @Test
  public void testGetColorToPersonCountJSONForListWithOnePerson() throws JsonProcessingException {
    Person person = new Person("Duck", "Donald", "", "99999", "(703)-742-0996", "Golden");
    List<Person> persons = new ArrayList<>();
    persons.add(person);
    ArrayNode actual = personServices.getColorToPersonCountJSON(persons);
    assertEquals(1, actual.size());
    assertEquals("Golden", actual.get(0).get("color").asText());
    assertEquals("1", actual.get(0).get("count").asText());
  }

  @Test
  public void testGetColorToPersonCountJSONForListWithTwoPersonsHavingSameColor() throws JsonProcessingException {
    Person person = new Person("Duck", "Donald", "", "99999", "(703)-742-0996", "Golden");
    Person person1 = new Person("Duck1", "Donald1", "", "99999", "(703)-742-0996", "Golden");
    List<Person> persons = new ArrayList<>();
    persons.add(person);
    persons.add(person1);
    ArrayNode actual = personServices.getColorToPersonCountJSON(persons);
    assertEquals(1, actual.size());
    assertEquals("Golden", actual.get(0).get("color").asText());
    assertEquals("2", actual.get(0).get("count").asText());
  }

  @Test
  public void testGetColorToPersonCountJSONForListWithTwoPersonsHavingDifferentColor() throws JsonProcessingException {
    Person person = new Person("Duck", "Donald", "", "99999", "(703)-742-0996", "Golden");
    Person person1 = new Person("Duck1", "Donald1", "", "99999", "(703)-742-0996", "Yellow");
    List<Person> persons = new ArrayList<>();
    persons.add(person);
    persons.add(person1);
    ArrayNode actual = personServices.getColorToPersonCountJSON(persons);
    assertEquals(2, actual.size());
    assertEquals("Golden", actual.get(1).get("color").asText());
    assertEquals("1", actual.get(1).get("count").asText());
    assertEquals("Yellow", actual.get(0).get("color").asText());
    assertEquals("1", actual.get(0).get("count").asText());
  }

  @Test
  public void testGetColorToPersonCountJSONForListWithFourPersons() throws JsonProcessingException {
    Person person = new Person("Duck", "Donald", "", "99999", "(703)-742-0996", "Golden");
    Person person1 = new Person("Duck1", "Donald1", "", "99999", "(703)-742-0996", "Yellow");
    Person person2 = new Person("Duck2", "Donald", "", "99999", "(703)-742-0996", "Red");
    Person person3 = new Person("Duck3", "Donald1", "", "99999", "(703)-742-0996", "Golden");
    List<Person> persons = new ArrayList<>();
    persons.add(person);
    persons.add(person1);
    persons.add(person2);
    persons.add(person3);
    ArrayNode actual = personServices.getColorToPersonCountJSON(persons);
    assertEquals(3, actual.size());
    assertEquals("Red", actual.get(0).get("color").asText());
    assertEquals("1", actual.get(0).get("count").asText());
    assertEquals("Yellow", actual.get(1).get("color").asText());
    assertEquals("1", actual.get(1).get("count").asText());
    assertEquals("Golden", actual.get(2).get("color").asText());
    assertEquals("2", actual.get(2).get("count").asText());
  }
}
