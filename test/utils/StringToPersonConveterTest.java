package utils;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import pojos.Person;

public class StringToPersonConveterTest {

  StringToPersonConveter converter = null;

  @Before
  public void setup() {
    converter = new StringToPersonConveter();
  }

  @Rule
  public ExpectedException expectedEx = ExpectedException.none();

  @Test(expected = Exception.class)
  public void testStringToPersonForEmptyString() throws Exception {
    converter.stringToPerson("");
  }

  @Test
  public void testStringToPersonForScenario1() throws Exception {
    Person person = new Person("Duck", "Donald", "", "99999", "(703)-742-0996", "Golden");
    assertEquals(person, converter.stringToPerson("Duck, Donald, (703)-742-0996, Golden, 99999"));
  }

  @Test
  public void testStringToPersonForScenario2() throws Exception {
    Person person = new Person("Donald", "Duck", "", "99999-1234", "703 955 0373", "Golden");
    assertEquals(person, converter.stringToPerson("Donald Duck, Golden, 99999-1234, 703 955 0373"));
  }

  @Test
  public void testStringToPersonForScenario3() throws Exception {
    Person person = new Person("Donald", "Duck", "", "99999", "646 111 0101", "Golden");
    assertEquals(person, converter.stringToPerson("Donald, Duck, 99999, 646 111 0101, Golden "));
  }

  @Test
  public void testStringToPersonForScenario4() throws Exception {
    Person person = new Person("Donald", "Duck", "1 Disneyland", "99999", "876-543-2104", "Golden");
    assertEquals(person, converter.stringToPerson("Donald Duck, 1 Disneyland, 99999, 876-543-2104, Golden"));
  }

}
