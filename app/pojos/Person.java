package pojos;

public class Person {

  public Person() {

  }

  public Person(String firstname, String lastname, String address, String zipcode, String phone, String color) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.address = address;
    this.zipcode = zipcode;
    this.phone = phone;
    this.color = color;
  }

  private String firstname = "";
  private String lastname = "";
  private String address = "";
  private String zipcode = "";
  private String phone = "";
  private String color = "";

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (!(obj instanceof Person)) {
      return false;
    }

    Person person = (Person) obj;
    return person.getFirstname().equals(this.firstname) && person.getLastname().equals(this.lastname)
        && person.getAddress().equals(this.address) && person.getPhone().equals(this.phone)
        && person.getZipcode().equals(this.zipcode) && person.getColor().equals(this.color);

  }

  public int hashCode() {
    return this.firstname.hashCode() + this.lastname.hashCode() + this.address.hashCode() + this.phone.hashCode()
        + this.zipcode.hashCode() + this.color.hashCode();
  }

}
