import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import play.Logger;
import pojos.Person;
import services.PersonServices;
import utils.StringToPersonConveter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;


public class MainRunner {

  public static void main(String[] args) {
    if (args[0] == null) {
      System.out.println("Please provide a file path as input");
    }
    Path path = Paths.get(args[0]);
    Charset charset = Charset.forName("ISO-8859-1");
    try {
      List<String> lines = Files.readAllLines(path, charset);
      PersonServices personServices = new PersonServices();
      StringToPersonConveter conveter = new StringToPersonConveter();
      List<Person> persons = new ArrayList<>();
      for (String line : lines) {
        try {
          persons.add(conveter.stringToPerson(line));
        } catch (Exception e) {
          Logger.warn(e.getMessage());
        }
      }
      System.out.println("---------print number of people that belong to each color,---------");
      printJson(personServices.getColorToPersonCountJSON(persons));
      System.out
          .println("---------print the color, the count and the names of people who belong to that color---------");
      printJson(personServices.getColorToPersonsAndCountJSON(persons));
    } catch (IOException e) {
      System.out.println(e);
    }
  }

  public static void printJson(ArrayNode arrayNode) {
    for (JsonNode node : arrayNode) {
      Iterator<String> iterator = node.fieldNames();
      while (iterator.hasNext()) {
        String key = iterator.next();
        JsonNode value = node.get(key);
        if (value instanceof ArrayNode) {
          for (JsonNode node1 : (ArrayNode) value) {
            System.out.print(node1.asText() + ",  ");
          }
        } else {
          System.out.print(value.asText() + "  ");
        }
      }
      System.out.println();
    }
  }

}
