package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import play.db.DB;
import pojos.Person;
import utils.ExceptionEnum;

@SuppressWarnings("deprecation")
public class PersonDAO {

  private final String selectQuery = "select * from Person";

  public List<Person> getAllPersons() throws Exception {
    List<Person> persons = new ArrayList<>();
    Connection connection = DB.getConnection();
    try {
      PreparedStatement statement = connection.prepareStatement(selectQuery);
      ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        Person person = new Person();
        person.setFirstname(rs.getString(1));
        person.setLastname(rs.getString(2));
        person.setAddress(rs.getString(3));
        person.setZipcode(rs.getString(4));
        person.setPhone(rs.getString(5));
        person.setColor(rs.getString(6));
        persons.add(person);
      }
    } catch (SQLException e) {
      throw new Exception(ExceptionEnum.SQL_EXCEPTION_SELECT_ALL_PERSON.name());
    }
    return persons;
  }

}
