package controllers;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.PersonServices;
import utils.ExceptionEnum;

import com.fasterxml.jackson.databind.node.ObjectNode;

import daos.PersonDAO;

public class PersonController extends Controller {

  public Result colorCountAndNames() {
    try {
      return ok(new PersonServices().getColorToPersonsAndCountJSON(new PersonDAO().getAllPersons()));
    } catch (Exception e) {
      return ok(getExceptionNode(e.getMessage()));
    }
  }

  public Result colorCount() {
    try {
      return ok(new PersonServices().getColorToPersonCountJSON(new PersonDAO().getAllPersons()));
    } catch (Exception e) {
      return ok(getExceptionNode(e.getMessage()));
    }
  }

  @SuppressWarnings("deprecation")
  private ObjectNode getExceptionNode(String exceptionMessage) {
    ExceptionEnum exceptionEnum = ExceptionEnum.valueOf(exceptionMessage);
    ObjectNode result = Json.newObject();
    result.put("success", false);
    ObjectNode meesageAndCode = Json.newObject();
    meesageAndCode.put("code", exceptionEnum.getCode());
    meesageAndCode.put("message", exceptionEnum.getDescription());
    result.put(exceptionEnum.getType(), meesageAndCode);
    return result;
  }
}
