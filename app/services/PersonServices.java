package services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pojos.Person;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class PersonServices {

  ObjectMapper mapper = null;

  public PersonServices() {
    mapper = new ObjectMapper();
    mapper.getFactory().configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
  }

  public ArrayNode getColorToPersonCountJSON(List<Person> persons) throws JsonProcessingException {
    Map<String, Integer> map = new HashMap<>();
    for (Person person : persons) {
      if (map.get(person.getColor()) != null) {
        map.put(person.getColor(), map.get(person.getColor()) + 1);
      } else {
        map.put(person.getColor(), 1);
      }

    }
    ArrayNode jsonArray = mapper.getNodeFactory().arrayNode();
    for (String color : map.keySet()) {
      ObjectNode node = mapper.getNodeFactory().objectNode();
      node.put("color", color);
      node.put("count", map.get(color));
      jsonArray.add(node);
    }
    return jsonArray;
  }

  @SuppressWarnings("deprecation")
  public ArrayNode getColorToPersonsAndCountJSON(List<Person> persons) throws JsonProcessingException {
    Map<String, Integer> colorCountMap = new HashMap<>();
    Map<String, List<String>> colorNamesMap = new HashMap<>();
    for (Person person : persons) {
      if (colorCountMap.get(person.getColor()) != null) {
        colorCountMap.put(person.getColor(), colorCountMap.get(person.getColor()) + 1);
      } else {
        colorCountMap.put(person.getColor(), 1);
      }
      List<String> names = null;
      if (colorNamesMap.get(person.getColor()) != null) {
        names = colorNamesMap.get(person.getColor());
      } else {
        names = new ArrayList<>();
      }
      names.add(person.getFirstname() + " " + person.getLastname());
      colorNamesMap.put(person.getColor(), names);
    }
    ArrayNode jsonArray = mapper.getNodeFactory().arrayNode();
    for (String color : colorCountMap.keySet()) {
      ObjectNode node = mapper.getNodeFactory().objectNode();
      node.put("color", color);
      node.put("count", colorCountMap.get(color));
      ArrayNode namesArray = mapper.getNodeFactory().arrayNode();
      for (String name : colorNamesMap.get(color)) {
        namesArray.add(name);
      }
      node.put("names", namesArray);
      jsonArray.add(node);
    }
    return jsonArray;
  }

}
