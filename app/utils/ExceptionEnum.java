package utils;

public enum ExceptionEnum {

  FIRST_NAME_MISSING(0, "firstname cannot be empty in the json", ExceptionType.ERROR),
  LAST_NAME_MISSING(1, "lastname cannot be empty in the json", ExceptionType.ERROR),
  ZIP_CODE_MISSING(2, "zipcode cannot be empty in the json", ExceptionType.ERROR),
  PHONE_NUMBER_MISSING(3, "number cannot be empty in the json", ExceptionType.ERROR),
  COLOR_MISSING(4, "color cannot be empty in the json", ExceptionType.ERROR),
  ADDRESS_MISSING(5, "You may provide the address in the json, it is an optional field", ExceptionType.WARNING), 
  SQL_EXCEPTION_INSERT_PERSON(6, "Sql exception occurred while inserting person", ExceptionType.ERROR), 
  SQL_EXCEPTION_SELECT_ALL_PERSON(7, "Sql exception occurred while retrieving all persons", ExceptionType.ERROR);
  
  private final int code; 
  private final String description;
  private final String type;

  private ExceptionEnum(int code, String description, String type) {
    this.code = code;
    this.description = description;
    this.type = type;
  }

  public String getDescription() {
     return description;
  }

  public int getCode() {
     return code;
  }

  public String getType() {
    return type;
  }
  
  private static class ExceptionType{
    private final static String ERROR = "error";
    private final static String WARNING = "warning";
  }
  
}
