package utils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pojos.Person;


public class StringToPersonConveter {

  final private Pattern scenario1Pattern =
      Pattern
          .compile("^([A-Z]\\w+)\\s*,\\s*([A-Z]\\w+)\\s*,\\s*(\\(\\d{3}\\)-\\d{3}-\\d{4})\\s*,\\s*([A-Z]\\w+)\\s*,\\s*(\\d{5})\\s*$");
  final private Pattern scenario2Pattern =
      Pattern
          .compile("^([A-Z]\\w+)\\s+([A-Z]\\w+)\\s*,\\s*([A-Z]\\w+)\\s*,\\s*(\\d{5}-\\d{4})\\s*,\\s*(\\d{3} \\d{3} \\d{4})\\s*$");
  final private Pattern scenario3Pattern = Pattern
      .compile("^([A-Z]\\w+)\\s*,\\s*([A-Z]\\w+)\\s*,\\s*(\\d{5})\\s*,\\s*(\\d{3} \\d{3} \\d{4}),\\s*([A-Z]\\w+)\\s*$");
  final private Pattern scenario4Pattern =
      Pattern
          .compile("^([A-Z]\\w+)\\s+([A-Z]\\w+)\\s*,\\s*(.*)\\s*,\\s*(\\d{5})\\s*,\\s*(\\d{3}-\\d{3}-\\d{4})\\s*,\\s*([A-Z]\\w+)\\s*$");

  public Person stringToPerson(String str) throws Exception {
    Person person = new Person();
    Matcher scenario1Matcher = scenario1Pattern.matcher(str);
    Matcher scenario2Matcher = scenario2Pattern.matcher(str);
    Matcher scenario3Matcher = scenario3Pattern.matcher(str);
    Matcher scenario4Matcher = scenario4Pattern.matcher(str);
    if (scenario1Matcher.find()) {
      person.setFirstname(scenario1Matcher.group(1));
      person.setLastname(scenario1Matcher.group(2));
      person.setPhone(scenario1Matcher.group(3));
      person.setColor(scenario1Matcher.group(4));
      person.setZipcode(scenario1Matcher.group(5));
    } else if (scenario2Matcher.find()) {
      person.setFirstname(scenario2Matcher.group(1));
      person.setLastname(scenario2Matcher.group(2));
      person.setPhone(scenario2Matcher.group(5));
      person.setColor(scenario2Matcher.group(3));
      person.setZipcode(scenario2Matcher.group(4));
    } else if (scenario3Matcher.find()) {
      person.setFirstname(scenario3Matcher.group(1));
      person.setLastname(scenario3Matcher.group(2));
      person.setPhone(scenario3Matcher.group(4));
      person.setColor(scenario3Matcher.group(5));
      person.setZipcode(scenario3Matcher.group(3));
    } else if (scenario4Matcher.find()) {
      person.setFirstname(scenario4Matcher.group(1));
      person.setLastname(scenario4Matcher.group(2));
      person.setAddress(scenario4Matcher.group(3));
      person.setPhone(scenario4Matcher.group(5));
      person.setColor(scenario4Matcher.group(6));
      person.setZipcode(scenario4Matcher.group(4));
    } else {
      throw new Exception("Not a valid string format");
    }
    return person;
  }

}
